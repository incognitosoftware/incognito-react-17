# Environment setup

Dependencies:
* node
* npm

Once completed with the environment setup \
run `npm install` to install the dependencies in the project directory

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `mvn clean install -Denv=test`
Builds with the configurations available in the `.env.test` configuration file. \
This will create a `war` file, which we can use to deploy the project in tomcat.

## Add a new deployment env
* create a new file similar to `.env.<new_env>` and replace the configurations
* Add a script to `package.json` -> `scripts` similar to this\
`build:<new_env>": "env-cmd -f .env.<new_env> npm run build"`
* Run `mvn clean install -Denv=<new_env>` to build for the new_env
