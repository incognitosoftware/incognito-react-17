import React    from 'react';
import ReactDOM from 'react-dom';
import Dx       from './init/Dx';
import {
  BrowserRouter
}               from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css'
import './css/index.css';
import './css/main.css';
import './fonts/Roboto-Regular.ttf';
import './fonts/Roboto-Bold.ttf';
import './fonts/Roboto-Italic.ttf';
import './fonts/RobotoCondensed-Regular.ttf';
import './fonts/RobotoCondensed-Bold.ttf';
import './fonts/RobotoCondensed-Italic.ttf';
import './fonts/NunitoSans-Regular.ttf';
import './fonts/NunitoSans-Bold.ttf';
import './fonts/NunitoSans-Italic.ttf';
import './i18n';

const headers =  {
  'Content-Type': 'application/json'
};

const language = 'en';

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <Dx headers={headers} language={language} />
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);
