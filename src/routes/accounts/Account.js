import React, { useEffect, useState } from 'react';
import Container from "react-bootstrap/Container";
import { find, merge } from 'lodash';
import { useHistory } from "react-router-dom";
import DxEntityHeader from "../../shared/common/DxEntityHeader";
import { dropdownMenuType } from "../../shared/types/dataTypes";
import AccountSubscribers from "./AccountSubscribers";
import { fetchData } from "../../shared/data/DataApi";
import DxSpinner from "../../shared/common/DxSpinner";
import { URL_ACCOUNT } from "../../constants/urls";
import AccountSubscriberDetails from "./AccountSubscriberDetails";
import AccountAdditionalInfo from "./AccountAdditionalInfo";
import AccountSubscriptions from "./AccountSubscriptions";
import AccountDevices from "./AccountDevices";
import DxAccordionBuilder from "../../shared/Accordion/DxAccordionBuilder";
import AccountAccordionJson from "../../json/accounts/accountAccordion.json";
import AccountSubscriberTableJson from '../../json/accounts/accountSubscriberTable.json';
import AccountSubscriptionTableJson from '../../json/accounts/accountSubscriptionTable.json';
import AccountDevicesTableJson from "../../json/accounts/accountDevicesTable.json";
import AccountSubscriptionModal from "./AccountSubscriptionModal";

const components = {
  AccountSubscriberDetails,
  AccountAdditionalInfo,
  AccountSubscribers,
  AccountSubscriptions,
  AccountDevices
};

const Account = (props) => {
  const [accountIdentifier, setAccountIdentifier] = useState(props.computedMatch.params.id);
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState(false);
  const [subscriber, setSubscriber] = useState(null);
  const [accordionJson, setAccordionJson] = useState(AccountAccordionJson);
  const [subscriberJson, setSubscriberJson] = useState([]);
  const [subscriptionJson, setSubscriptionJson] = useState(AccountSubscriptionTableJson);
  const [deviceJson, setDeviceJson] = useState(AccountDevicesTableJson);
  const [showSubscriptionModal, setShowSubscriptionModal] = useState(false);
  const [selectedSubscription, setSelectedSubscription] = useState(null);

  const history = useHistory();

  useEffect(() => {
    initializeState();
  }, []);

  useEffect(() => {
    loadData();
  }, [accountIdentifier]);

  useEffect(() => {
    populateAccountAccordionJson();
  }, [data, subscriber, subscriberJson, subscriptionJson, deviceJson]);

  useEffect(() => {
    if (data.identifier) {
      setAccountIdentifier(data.identifier);
      setSubscriber(null);
    }
  }, [data]);

  const loadData = async () => {
    setLoading(true);
    try {
      const URL = `${URL_ACCOUNT}/${accountIdentifier}`;
      const dataResp = await fetchData(URL);
      setLoading(false);

      if (dataResp.error) {
        // todo: show error
        return;
      }
      setData(dataResp.data);
    } catch (e) {
      setLoading(false);
      // todo: show error
    }
  };

  const initializeState = () => {
    const subscriberJsonCloned = JSON.parse(JSON.stringify(AccountSubscriberTableJson));
    const actions = find(subscriberJsonCloned, { systemName: 'subscriberAction' });
    actions.dataType.onClick = onClickSubscriberAction;
    setSubscriberJson(subscriberJsonCloned);
  }

  const getPropsForComponents = (componentName) => {
    switch (componentName) {
      case 'AccountSubscribers':
        return {
          data: data && data.subscribers ? data.subscribers : [],
          onSelectRow: onSelectSubscriberRow,
          columns: subscriberJson
        };
      case 'AccountSubscriberDetails':
        return { data: subscriber };
      case 'AccountAdditionalInfo':
        return { data: subscriber };
      case 'AccountSubscriptions':
        return {
          data: subscriber ? subscriber.services : [],
          columns: subscriptionJson,
          onDoubleClickRow: onDoubleClickSubscription
        };
      case 'AccountDevices':
        return { data: subscriber ? subscriber.devices : [], columns: deviceJson };
      default:
        return {};
    }
  }

  const onDoubleClickSubscription = (id) => {
    if (!subscriber || !subscriber.services || !subscriber.services.length) {
      setSelectedSubscription(null);
      return;
    }

    const subscription = find(subscriber.services, { id: 3 });
    if (!subscription) {
      setSelectedSubscription(null);
      return;
    }

    setSelectedSubscription(subscription);
    setShowSubscriptionModal(true);
  }

  const onSelectSubscriberRow = (rowIds) => {
    if (!rowIds || !rowIds.length) {
      setSubscriber(null);
      return;
    }

    setSubscriber(find(data.subscribers, { id: rowIds[0] }));
  };

  const headerActions = [
    {
      type: dropdownMenuType.HEADING,
      name: 'Actions'
    },
    {
      type: dropdownMenuType.DIVIDER
    },
    {
      name: "Create a New Account",
      type: dropdownMenuType.ACTION,
      onClick: () => {
        console.log('create new account')
      },
    },
    {
      name: "Suspend Account",
      type: dropdownMenuType.ACTION,
      onClick: () => {
        console.log('Suspend Account')
      },
    },
    {
      name: "Delete Account",
      type: dropdownMenuType.ACTION,
      onClick: () => {
        console.log('delete account')
      },
    },
    {
      name: "Save",
      type: dropdownMenuType.ACTION,
      onClick: () => {
        console.log('save')
      },
    }
  ];

  const onColumnVisibilityChange = (component, column) => {
    let columnJson;
    let stateFunction;
    switch (component) {
      case 'subscriber':
        columnJson = JSON.parse(JSON.stringify(subscriberJson));
        stateFunction = setSubscriberJson;
        break;
      case 'devices':
        columnJson = JSON.parse(JSON.stringify(deviceJson));
        stateFunction = setDeviceJson;
        break;
      case 'subscriptions':
        columnJson = JSON.parse(JSON.stringify(subscriptionJson));
        stateFunction = setSubscriptionJson;
        break;
      default:
        return null;
    }

    const selectedColumn = find(columnJson, { systemName: column.systemName });
    if (selectedColumn) {
      selectedColumn.visible = column.visible;
      stateFunction(columnJson);
    }
  }

  const onClickSubscriberAction = (action, rowId, row) => {
    console.log(action, rowId, row)
    if (action === 'Action 1') {
      //todo: perform the action
    }
  }

  const getColumn = (component) => {
    switch (component) {
      case 'subscriber':
        return {
          columns: subscriberJson,
          onColumnVisibilityChange: (column) => onColumnVisibilityChange(component, column)
        };
      case 'devices':
        return {
          columns: deviceJson,
          onColumnVisibilityChange: (column) => onColumnVisibilityChange(component, column)
        };
      case 'subscriptions':
        return {
          columns: subscriptionJson,
          onColumnVisibilityChange: (column) => onColumnVisibilityChange(component, column)
        };
      default:
        return null;
    }
  }

  const populateAccountAccordionJson = () => {
    const updatedAccordionJson = JSON.parse(JSON.stringify(accordionJson));
    updatedAccordionJson.forEach(accordion => {
      if (accordion.systemName !== 'subscriber') {
        accordion.expanded = !!subscriber;
        accordion.disabled = !subscriber;
      }

      if (accordion.systemName === 'subscriberDetails' && subscriber) {
        accordion.title = subscriber.lastName;
      }

      merge(accordion, getColumn(accordion.systemName));
      const Component = components[accordion.component.name];
      accordion.body = <Component {...getPropsForComponents(accordion.component.name)}/>;
    });
    setAccordionJson(updatedAccordionJson);
  }

  const onCloseSubscriptionModal = () => {
    setSelectedSubscription(null);
    setShowSubscriptionModal(false);
  }

  const onSearchAccount = async (searchText) => {
    if (!searchText) {
      return;
    }

    history.replace({
      pathname: '/dx/accounts',
      state: {
        searchValue: searchText,
        navigateOnOneMatch: true
      }
    });
  }

  return (
    <div className="incognito-route-container">
      <DxEntityHeader
        module={'Account'}
        title={accountIdentifier}
        onSearch={onSearchAccount}
        onRefresh={() => loadData()}
        searchPlaceholder='Search another account'
        actions={headerActions}
      />
      <div className="incognito-page-container gray top-spacing">
        <Container>
          <DxSpinner visible={loading}/>
          <DxAccordionBuilder accordionCollection={accordionJson}/>
        </Container>
        <AccountSubscriptionModal data={selectedSubscription} showModal={selectedSubscription && showSubscriptionModal}
                                  onClose={onCloseSubscriptionModal}/>
      </div>
    </div>
  );
};

export default Account;
