import React from 'react';
import { useLocation } from 'react-router-dom';
import { URL_ACCOUNTS } from "../../constants/urls";
import AccountTableJson from '../../json/accounts/accountTable.json';
import ModuleTableView from "../../shared/commonViews/ModuleTableView";

const Accounts = (props) => {

  const location = useLocation();

  const formatData = (accounts) => {
    return accounts.map(res => {
      res.subscriber = res.subscribers[0];
      return res;
    });
  }

  const onClickCreate = () => {
    console.log('create');
  }

  const onClickExportAllResults = () => {
    console.log('onClickExportAllResults');
  }

  const onClickSuspend = () => {
    console.log('onClickSuspend');
  }

  const onClickDelete = () => {
    console.log('onClickDelete');
  }

  const buttons = [
    {
      title: 'Create',
      onClick: onClickCreate,
      disabled: false
    },
    {
      title: 'Export All Results',
      onClick: onClickExportAllResults,
      disabled: false
    },
    {
      title: 'Suspend',
      onClick: onClickSuspend,
      disabled: true
    },
    {
      title: 'Delete',
      onClick: onClickDelete,
      disabled: true
    }
  ];

  const getQuery = () => {
    if (!location.state || !location.state.searchValue) {
      return null;
    }

    return `ANYFIELD%3D${location.state.searchValue}*`
  }

  return <ModuleTableView
    module={'accounts'}
    title={'ACCOUNTS'}
    tableJson={AccountTableJson}
    history={props.history}
    url={URL_ACCOUNTS}
    query={getQuery()}
    navigateOnOneMatch={location.state ? !!location.state.navigateOnOneMatch : false}
    formatResponseData={formatData}
    headerButtons={buttons}
    enableDoubleClick
  />;
}

export default Accounts;
