import React from 'react';
import { useTranslation } from 'react-i18next';
import DxTableBuilder from "../../shared/tables/DxTableBuilder";

const AccountDevices = ({ data, columns }) => {
  const { t } = useTranslation();

  return (
    <DxTableBuilder
      columns={columns}
      rows={data}
      height={'100%'}
      multiSelect={false}
      totalRecordsCount={data.length}
    />
  );
}

export default AccountDevices;
