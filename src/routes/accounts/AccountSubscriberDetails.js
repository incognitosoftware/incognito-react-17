import React, { useEffect, useState } from 'react';
import DxFormBuilder from "../../shared/forms/DxFormBuilder";
import accountSubscriberDetailsJSON from "../../json/accounts/accountSubscriberDetails.json";
import { findIndexByName } from "../../shared/utils/CommonUtils";

const AccountSubscriberDetails = ({ data }) => {
  const [form, setForm] = useState(accountSubscriberDetailsJSON);

  useEffect(() => {
    if (data) {
      populateFormValues();
    } else {
      setForm(accountSubscriberDetailsJSON);
    }
  }, [data]);

  const populateFormValues = () => {
    const latestForm = JSON.parse(JSON.stringify(accountSubscriberDetailsJSON));
    latestForm[
      findIndexByName(latestForm, 'systemName', 'subscriberName')].value = data.lastName || '';
    latestForm[
      findIndexByName(latestForm, 'systemName', 'homePhone')].value = data.homePhoneNumber || '';
    latestForm[
      findIndexByName(latestForm, 'systemName', 'subscriberType')].value = data.type || '';
    latestForm[
      findIndexByName(latestForm, 'systemName', 'subscriberNumber')].value = data.identifier || '';
    setForm(latestForm);
  }

  return (
    <DxFormBuilder fields={form}/>
  );
};

export default AccountSubscriberDetails;
