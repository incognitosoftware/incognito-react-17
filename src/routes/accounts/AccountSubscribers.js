import React from 'react';
import { useTranslation } from 'react-i18next';
import DxTableBuilder from "../../shared/tables/DxTableBuilder";

const AccountSubscribers = ({ data, onSelectRow, columns }) => {
  const { t } = useTranslation();

  const onSelectRows = (rows) => {
    if (typeof onSelectRow === 'function') {
      onSelectRow(rows);
    }
  }

  return (
    <DxTableBuilder
      columns={columns}
      rows={data}
      height={'100%'}
      multiSelect={false}
      totalRecordsCount={data.length}
      onSelectRows={onSelectRows}
    />
  );

}

export default AccountSubscribers;
