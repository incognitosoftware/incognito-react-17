import React from 'react';
import { useTranslation } from 'react-i18next';
import DxTableBuilder from "../../shared/tables/DxTableBuilder";

const AccountSubscriptions = ({ data, columns, onDoubleClickRow }) => {
  const { t } = useTranslation();

  return (
    <DxTableBuilder
      columns={columns}
      rows={data}
      height={'100%'}
      multiSelect={false}
      totalRecordsCount={data.length}
      onDoubleClickRow={onDoubleClickRow}
    />
  );

}

export default AccountSubscriptions;
