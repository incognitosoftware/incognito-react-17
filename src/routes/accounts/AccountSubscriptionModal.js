import React, { useEffect, useState } from 'react';
import DxModalBuilder from "../../shared/modals/DxModalBuilder";
import DxFormBuilder from "../../shared/forms/DxFormBuilder";
import accountSubscriptionFormJSON from "../../json/accounts/accountSubscriptionForm.json";
import { findIndexByName } from "../../shared/utils/CommonUtils";
import { modalSize } from "../../shared/types/dataTypes";

const AccountSubscriptionModal = ({ showModal, data, onClose }) => {
  const [form, setForm] = useState(accountSubscriptionFormJSON);

  useEffect(() => {
    if (data) {
      populateFormValues();
    } else {
      setForm(accountSubscriptionFormJSON);
    }
  }, [data]);

  const populateFormValues = () => {
    const latestForm = JSON.parse(JSON.stringify(accountSubscriptionFormJSON));
    latestForm[
      findIndexByName(latestForm, 'systemName', 'identifier')].value = data.identifier || '';
    latestForm[
      findIndexByName(latestForm, 'systemName', 'status')].value = data.status || '';
    latestForm[
      findIndexByName(latestForm, 'systemName', 'description')].value = data.description || '';
    latestForm[
      findIndexByName(latestForm, 'systemName', 'device')].value = data.device &&
    data.device.identifier ? data.device.identifier : '';
    setForm(latestForm);
  }

  const modalButtonsRight = [
    {
      title: 'Cancel',
      onClick: onClose,
      disabled: false
    },
    {
      title: 'Suspend',
      onClick: () => {
      },
      disabled: false
    },
    {
      title: 'Schedule Service',
      onClick: () => {
      },
      disabled: true
    },
    {
      title: 'Save',
      onClick: () => {
      },
      disabled: true
    }
  ];
  const modalButtonsLeft = [
    {
      title: 'Delete',
      onClick: () => {
      },
      disabled: false,
      variant: 'danger'
    }
  ];

  return (
    <DxModalBuilder
      modalConfig={{
        title: data && data.serviceType ? data.serviceType : '',
        subTitle: 'SUBSCRIPTION',
        body: <DxFormBuilder fields={form}/>,
        rightButtons: modalButtonsRight,
        leftButtons: modalButtonsLeft,
        size: modalSize.LARGE
      }}
      showModal={showModal}
      onClose={onClose}
    />
  );
};

export default AccountSubscriptionModal;
