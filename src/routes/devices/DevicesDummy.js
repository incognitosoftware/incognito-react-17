import React from 'react';
import DevicesTableJson from '../../json/devices/devicesTable.json';
import Container from "react-bootstrap/Container";
import DxTableBuilder from "../../shared/tables/DxTableBuilder";
import useLayoutHeight from "../../shared/hooks/useLayoutHeight";

const DevicesDummy = (props) => {
  const totalPaddingOfContainer = 25;
  const { layoutHeight, layoutRef } = useLayoutHeight(totalPaddingOfContainer);
  const onClickCreate = () => {
    console.log('create');
  }

  const onClickExportAllResults = () => {
    console.log('onClickExportAllResults');
  }

  const onClickLockUnlockDevices = () => {
    console.log('onClickLockUnlockDevices');
  }

  const onClickSwapDevices = () => {
    console.log('onClickSwapDevices');
  }

  const buttons = [
    {
      title: 'Create',
      onClick: onClickCreate,
      disabled: false
    },
    {
      title: 'Export All Results',
      onClick: onClickExportAllResults,
      disabled: false
    },
    {
      title: 'Lock/Unlock Device',
      onClick: onClickLockUnlockDevices,
      disabled: true
    },
    {
      title: 'Swap Device',
      onClick: onClickSwapDevices,
      disabled: true
    }
  ];

  const data = [];
  for (let i = 0; i < 1000; i++) {
    data.push({
      identifier: i
    })
  }
  return <div className="incognito-route-container">
    <div className="incognito-page-container top-spacing" ref={layoutRef}>
      <Container>
        <DxTableBuilder
          columns={DevicesTableJson}
          rows={data}
          height={'100%'}
          multiSelect={true}
          paginationEnabled
          layoutHeight={layoutHeight}
          enableVirtualization
        />
      </Container>
    </div>
  </div>;
}

export default DevicesDummy;
