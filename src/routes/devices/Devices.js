import React from 'react';
import { URL_DEVICES } from "../../constants/urls";
import DevicesTableJson from '../../json/devices/devicesTable.json';
import ModuleTableView from "../../shared/commonViews/ModuleTableView";

const Devices = (props) => {
  const onClickCreate = () => {
    console.log('create');
  }

  const onClickExportAllResults = () => {
    console.log('onClickExportAllResults');
  }

  const onClickLockUnlockDevices = () => {
    console.log('onClickLockUnlockDevices');
  }

  const onClickSwapDevices = () => {
    console.log('onClickSwapDevices');
  }

  const buttons = [
    {
      title: 'Create',
      onClick: onClickCreate,
      disabled: false
    },
    {
      title: 'Export All Results',
      onClick: onClickExportAllResults,
      disabled: false
    },
    {
      title: 'Lock/Unlock Device',
      onClick: onClickLockUnlockDevices,
      disabled: true
    },
    {
      title: 'Swap Device',
      onClick: onClickSwapDevices,
      disabled: true
    }
  ];

  return <ModuleTableView
    module={'devices'}
    title={'DEVICES'}
    tableJson={DevicesTableJson}
    history={props.history}
    url={URL_DEVICES}
    headerButtons={buttons}
  />;
}

export default Devices;
