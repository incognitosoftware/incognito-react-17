import React     from 'react'
import Container from 'react-bootstrap/Container'

const DevicesDashboard = (props) => {

  return (
    <div className="incognito-route-container">
        <Container>
          <div className="incognito-app-header">
            <div className="incognito-app-title">Device Dashboard</div>
            <div className="incognito-app-logo-header">
              <div className="incognito-app-filters">Filters and search here</div>
            </div>
          </div> 
        </Container>
      <div className="incognito-page-container gray top-spacing">
        <Container>

        </Container>
      </div>
    </div>
  );
  
}

export default DevicesDashboard;
