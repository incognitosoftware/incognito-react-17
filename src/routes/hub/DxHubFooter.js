const DxHubFooter = (props) => {

  return (
    <div>
      <div className="incognito-hub-footer">
        <div className={`${props.config ? props.config.className : null}`}>{props.config ? props.config.text : null}</div>
        {props.config && props.config.img ? <div className={props.config.img}/> : null}
      </div>
    </div>
  );
}

export default DxHubFooter;
