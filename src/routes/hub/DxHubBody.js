import DxSearch from './DxSearch'
import DxHubAlert from "./DxHubAlert";
import DxHubList from "./DxHubList";

const DxHubBody = (props) => {

  return (
    <div className="incognito-hub-body">
      {props.config.type === 'text' ? <div className="incognito-hub-body-text">{props.config.text}</div> : null}
      {props.config.type === 'search' ? <DxSearch {...props} className={props.config.badgeColor} device={props.config.device}/> : null}
      {props.config.type === 'alert' ? <DxHubAlert/> : null}
      {props.config.type === 'list' ? <DxHubList campaign={props.config.campaign}/> : null}
    </div>
  );
}

export default DxHubBody;
