import React from 'react'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import DxHubPanel from './DxHubPanel'
import DxHubSpacer from './DxHubSpacer'
import hubForm from '../../json/hub/hub.json';

const DxHub = (props) => {

  const renderHub = () => {
    const rows = [];

    hubForm.layout.forEach((row, ridx) => {
      const panels = [];

      row.children.forEach((panel, pidx) => {
        if (panel.component === 'DxHubPanel') {
          panels.push(<DxHubPanel {...props} key={pidx} panel={panel}/>);
        } else {
          panels.push(<DxHubSpacer {...props} key={pidx}/>);
        }
      });

      rows.push(<Row key={ridx}>{panels}</Row>)
    });

    return rows;
  }

  return (
    <div className="incognito-route-container">
      <Container>
        <div className="incognito-app-hub-header">
          <div className={'incognito-app-title-left'}>
            <div className="incognito-app-title">Incognito</div>
            <div className="incognito-app-logo-header">
              <div className="incognito-app-logo"/>
              <div className="incognito-app-name">IoT Control Center</div>
            </div>
          </div>
          <div className={'incognito-app-title-right'}/>
        </div>
      </Container>
      <div className="incognito-page-container gray top-spacing">
        <Container>
          {renderHub()}
        </Container>
      </div>
    </div>
  );
}

export default DxHub;
