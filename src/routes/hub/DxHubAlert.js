import React from 'react';
import { ListGroup } from "react-bootstrap";

const DxHubAlert = (props) => {
  return (
    <div className="incognito-hub-search">
      <div className="incognito-hub-search-form">
        <div className="incognito-form-label">New Alerts</div>
        <ListGroup className="incognito-alert-list">
          <ListGroup.Item className="incognito-alert-list-item incognito-alert-list-item-red">4 New Alerts</ListGroup.Item>
        </ListGroup>
      </div>
      <div  className="incognito-hub-search-recent">
        <div className="incognito-form-label">
          Alert Rules
        </div>
          <ListGroup className="incognito-alert-list">
            <ListGroup.Item className="incognito-alert-list-item incognito-alert-list-item-blue">3 Global Rules</ListGroup.Item>
            <ListGroup.Item className="incognito-alert-list-item incognito-alert-list-item-blue">4 Device Rules</ListGroup.Item>
            <ListGroup.Item className="incognito-alert-list-item incognito-alert-list-item-blue">6 System Rules</ListGroup.Item>
          </ListGroup>
      </div>
    </div>
  );
};

export default DxHubAlert;
