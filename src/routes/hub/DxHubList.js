import React from 'react';
import { ListGroup } from "react-bootstrap";

const DxHubList = (props) => {
  return (
    <div className="incognito-hub-search">
      <div className="incognito-hub-list">
        <ListGroup className="incognito-alert-list">
          {
            props.campaign ? [<ListGroup.Item className="incognito-alert-list-item incognito-alert-list-item-blue">15 In
                Progress</ListGroup.Item>,
              <ListGroup.Item className="incognito-alert-list-item incognito-alert-list-item-green">0
                Scheduled</ListGroup.Item>] :
              [
                <ListGroup.Item className="incognito-alert-list-item incognito-alert-list-item-green">9 Enabled</ListGroup.Item>,
                <ListGroup.Item className="incognito-alert-list-item incognito-alert-list-item-orange">6 Disabled</ListGroup.Item>
              ]
          }

        </ListGroup>
      </div>
    </div>
  );
};

export default DxHubList;
