import React       from 'react'
import { useHistory } from 'react-router-dom';
import Col         from 'react-bootstrap/Col'
import DxHubHeader from "./DxHubHeader"
import DxHubBody   from "./DxHubBody"
import DxHubFooter from "./DxHubFooter"

const DxHubPanel = (props) => {

  const history = useHistory();
  const handleOnClick = () => {
    history.push({
      pathname: props.panel.url
    });
  };

  return (
    <Col xs={props.panel.size.xs} lg={props.panel.size.lg}>
      <div className="incognito-hub-panel" id={`incognito-hub-panel-${props.panel.header.title}`} onClick={(e) => {
        const classAttribute = e.target.getAttribute('class');
        if (classAttribute && (classAttribute.includes('form-control') || classAttribute.includes('incognito-hub-badge'))) {
          return;
        }
        handleOnClick();
      }}>
        <DxHubHeader {...props} config={props.panel.header}/>
        <DxHubBody {...props} config={props.panel.body} title={props.panel.header.title}/>
        <DxHubFooter {...props} config={props.panel.footer}/>
      </div>
    </Col>
  );
}

export default DxHubPanel;
