const DxHubHeader = (props) => {

  return (
    <div className="incognito-hub-header">
      <div className="incognito-hub-title">{props.config.title}</div>
    </div>
  );
}

export default DxHubHeader;