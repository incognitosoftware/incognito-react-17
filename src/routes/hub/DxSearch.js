import Form from 'react-bootstrap/Form'
import Badge from 'react-bootstrap/Badge'
import { useHistory } from "react-router-dom";
import { useState } from "react";

const DxSearch = (props) => {
    const [searchText, setSearchText] = useState('');
    const history = useHistory();

    const onSubmit = (e) => {
        e.preventDefault();
        if (!searchText) {
            return;
        }

        history.push({
            pathname: props.panel.url,
            state: {
                searchValue: searchText,
                navigateOnOneMatch: true
            }
        });
    };

    return (
      <div className="incognito-hub-search">
          <div className="incognito-hub-search-form">
              <Form noValidate onSubmit={onSubmit}>
                  <Form.Group>
                      <Form.Label className="incognito-form-label">Search</Form.Label>
                      <Form.Control onChange={e => setSearchText(e.target.value)}/>
                  </Form.Group>
              </Form>
          </div>
          <div className="incognito-hub-search-recent">
              <div className="incognito-form-label form-label">
                  Recent
              </div>
              <div>
                  <Badge className={["incognito-hub-badge", "incognito-hub-badge-1", props.className]}>
                      {props.device ? 'MQTT100' : '068-1860-5239'}
                  </Badge>
                  <Badge className={["incognito-hub-badge", "incognito-hub-badge-2", props.className]}>
                      {props.device ? 'MQTT101' : '069-7660-5240'}
                  </Badge>
                  <Badge className={["incognito-hub-badge", "incognito-hub-badge-3", props.className]}>
                      {props.device ? 'MQTT102' : '070-4560-5249'}
                  </Badge>
                  <Badge className={["incognito-hub-badge", "incognito-hub-badge-4", props.className]}>
                      {props.device ? 'MQTT103' : '213-5323-4322'}
                  </Badge>
              </div>
          </div>
      </div>
    );

}

export default DxSearch;
