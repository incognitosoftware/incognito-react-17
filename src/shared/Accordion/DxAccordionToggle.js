import React from 'react';
import { Button } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretRight, faCaretDown } from "@fortawesome/free-solid-svg-icons";

const DxAccordionToggle = ({ expanded, onClickToggle, disabled }) => {
  return (
    <Button
      variant="link"
      onClick={disabled ? () => {
      } : () => onClickToggle(!expanded)}
      disabled={disabled}
    >
      <FontAwesomeIcon
        size="xl"
        icon={expanded ? faCaretDown : faCaretRight}
        className="incognito-action-dropdown-toggle"/>
    </Button>
  );
};

export default DxAccordionToggle;
