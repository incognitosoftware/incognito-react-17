import React, { useEffect, useState } from 'react';
import { Accordion, Card } from "react-bootstrap";
import DxAccordionToggle from "./DxAccordionToggle";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faRotate } from "@fortawesome/free-solid-svg-icons";
import ActionDropdown from "../dropdowns/ActionDropdown";
import ColumnSelectionDropdown from "../dropdowns/ColumnSelectionDropdown";

const DxAccordion = ({
                       title,
                       expanded,
                       disabled = false,
                       onRefresh,
                       actions,
                       columns,
                       onColumnVisibilityChange,
                       body
                     }) => {
  const [isExpanded, setIsExpanded] = useState(expanded);

  useEffect(() => {
    setIsExpanded(expanded);
  }, [expanded]);

  return (
    <Accordion activeKey={isExpanded ? '0' : ''} className='incognito-accordion'>
      <Card className="incognito-accordion-card">
        <Card.Header
          className={`incognito-accordion-card-header${isExpanded ? '' : '-collapsed'}`}
        >
          <div className="incognito-title-container">
            <div className="incognito-accordion-heading-sub">
              <DxAccordionToggle disabled={disabled} expanded={isExpanded}
                                 onClickToggle={(state) => setIsExpanded(state)}/>
              <div className={`incognito-accordion-heading-text ${disabled && 'incognito-disabled'}`}>{title}</div>
            </div>
            <div className="incognito-accordion-heading-sub">
              {
                typeof onRefresh === "function" && !disabled &&
                <Link
                  className="incognito-app-entity-header-right-refresh"
                  onClick={onRefresh}
                >
                  <FontAwesomeIcon size='xl' icon={faRotate}/>
                </Link>
              }
              {
                actions && actions.length && !disabled &&
                <div className="incognito-app-entity-header-right-actions">
                  <ActionDropdown
                    actions={actions}
                  />
                </div>
              }
              {
                columns && columns.length && !disabled &&
                <div className="incognito-app-entity-header-right-actions">
                  <ColumnSelectionDropdown
                    columns={columns}
                    onColumnVisibilityChange={onColumnVisibilityChange}
                  />
                </div>
              }
            </div>
          </div>
        </Card.Header>
        <Accordion.Collapse eventKey="0" className="incognito-accordion-body">
          <Card.Body>
            {body}
          </Card.Body>
        </Accordion.Collapse>
      </Card>
    </Accordion>
  );
};

export default DxAccordion;
