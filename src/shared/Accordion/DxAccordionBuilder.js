import React from 'react';
import DxAccordion from "./DxAccordion";

const DxAccordionBuilder = ({ accordionCollection = [] }) => {
  const getProps = (accordionRes) => {
    const props = {};
    if (!accordionRes) {
      return {};
    }

    if (typeof accordionRes.onRefresh === 'function') {
      props.onRefresh = accordionRes.onRefresh;
    }

    if (accordionRes.actions && accordionRes.actions.length) {
      props.actions = accordionRes.actions;
    }

    if (accordionRes.columns && accordionRes.columns.length) {
      props.columns = accordionRes.columns;
    }

    if (typeof accordionRes.onColumnVisibilityChange === "function") {
      props.onColumnVisibilityChange = accordionRes.onColumnVisibilityChange;
    }

    return props;
  }

  return (
    <>
      {
        accordionCollection.map((accordion => {
          return <DxAccordion
            disabled={accordion.disabled}
            expanded={accordion.expanded}
            title={accordion.title}
            body={accordion.body}
            {...getProps(accordion)}
          />
        }))
      }
    </>
  );
};

export default DxAccordionBuilder;
