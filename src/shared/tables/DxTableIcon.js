import { translateText } from "../utils/TranslateUtils";

const DxTableIcon = ({ column, content, url }) => {
  return (
    <div style={{ width: "100%" }}>
      <img
        style={{ display: "inline-block" }}
        title={translateText(content)}
        src={url.icon}
        alt={translateText(content)}
      />
      {
        column.showText ?
          <div style={{ display: "inline-block", paddingLeft: "10px" }}>
            {translateText(content.toString())}
          </div> : null
      }
    </div>
  );
}

export default DxTableIcon;
