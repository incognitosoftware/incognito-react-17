import { columnDataTypes, dropdownMenuType, typeOfData } from '../types/dataTypes';
import { getFormattedTime, getFormattedDate, getDigitalTime } from "../utils/DateTimeUtils";
import { translateText } from "../utils/TranslateUtils";
import DxTableIcon from "./DxTableIcon";
import DxTableCell from "./DxTableCell";
import { forwardRef } from "react";
import ActionDropdown from "../dropdowns/ActionDropdown";

const DxTableRow = forwardRef(((
  {
    columns,
    row,
    rowIndex,
    onClick,
    onDoubleClick,
    selected
  }, ref) => {
  const getRow = () => {
    if (!columns || !columns.length || !row) {
      return null;
    }

    if (row.hasOwnProperty('visible') && !row.visible) {
      return null;
    }

    const cells = [];
    columns.forEach((column, index) => {
      if (column.hasOwnProperty('searchLoc') && column.searchLoc === 'table') {
        return null;
      }

      let content = parseCellContent(column, row, rowIndex);
      content = content === undefined || content === null ? '-' : content;
      cells.push(<DxTableCell
        key={`cell-${row.id}-${index}`}
        content={content}
        column={column}
        className={row.meta ? row.meta[`${column.name}_className`] : null}
      />);
    });

    return (
      <tr
        ref={ref}
        id={row.id}
        className={selected ? 'selected' : ''}
        onClick={(e) => {
          const idAttribute = e.target.getAttribute('id');
          if (idAttribute && idAttribute.includes('dropdown')) {
            return;
          }

          if (typeof onClick === "function") {
            onClick(row.id);
          }
        }}
        onDoubleClick={() => {
          if (typeof onDoubleClick === "function") {
            onDoubleClick(row.id);
          }
        }}
      >
        {cells}
      </tr>
    );
  }

  const getCellContent = (column, row) => {
    const sysNameField = column.hasOwnProperty('systemName') ? 'systemName' : 'name';

    if (column[sysNameField].indexOf('[') >= 0) {
      const multiples = column[sysNameField].replace(/(^.*\[|].*$)/g, '');
      const topField = column[sysNameField].split('.')[0];
      const fields = multiples.split('.');

      let cellContent = '';
      fields.forEach(field => {
        cellContent += row[topField][field] + ' '
      });
      return cellContent;
    }

    if (column[sysNameField].indexOf('.') >= 0) {
      const levels = column[sysNameField].split('.');

      if (row[levels[0]]) {
        return row[levels[0]][levels[1]];
      }
      return '';
    }

    return row[column[sysNameField]];
  }

  const parseCellContent = (column, row, idx) => {
    const content = getCellContent(column, row);

    switch (column.dataType.type) {
      case columnDataTypes.COUNTER:
        return idx;

      case columnDataTypes.DATE:
        return getFormattedDate(content);

      case columnDataTypes.TIME:
        return getFormattedTime(content);

      case columnDataTypes.DATETIME:
        const date = getFormattedDate(content);
        const time = getFormattedTime(content);
        return `${date} ${time}`;

      case columnDataTypes.DURATION:
        return getDigitalTime(content);

      case columnDataTypes.TEXT:
        if (typeof content === typeOfData.STRING) {
          return translateText(content);
        }
        return content;

      case columnDataTypes.ICON:
        const iconUrl = column.dataType.choice.find(choice => choice.systemName === content);
        if (!iconUrl) {
          return content;
        }
        return <DxTableIcon content={content} column={column} url={iconUrl}/>;

      case columnDataTypes.ICON_RANGE:
        const field = column.dataType.field;
        const choices = column.dataType.choice;
        let url;
        let range;

        for (let i = 0; i < choices.length; i++) {
          range = choices[i].systemName.split(' ');

          if (parseFloat(row[field]) > range[0] && parseFloat(row[field]) < range[1]) {
            url = choices[i];
            break;
          }
        }

        if (url) {
          return (<DxTableIcon column={column} url={url} content={''}/>);
        }
        return '';

      case columnDataTypes.ENUM:
        if (content && content.charAt(10) === 'T' && content.charAt(23) === 'Z') {
          const date = getFormattedDate(content);
          const time = getFormattedTime(content);

          return `${date}, ${time}`;
        }

        return content;

      case columnDataTypes.ACTIONS:
        return <ActionDropdown
          actions={column.dataType.actions ? column.dataType.actions : []}
          onClickAction={(actionName) => {
            if (typeof column.dataType.onClick === 'function') {
              column.dataType.onClick(actionName, row.id, row);
            }
          }}
        />
      default:
        return content;
    }
  }

  return getRow();
}));

export default DxTableRow;
