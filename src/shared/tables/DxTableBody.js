import { forwardRef, useCallback, useEffect, useRef, useState } from 'react';
import { indexOf, map, pull } from 'lodash';
import DxTableRow from './DxTableRow';
import { TABLE_SELECT_TYPES } from "../../constants/enums";

const DxTableBody = forwardRef((
  {
    columns,
    rows,
    multiSelect,
    selectType,
    onChangeSelectType = () => {
    },
    currentPage,
    loading,
    onPageChange,
    hasMore,
    onDoubleClickRow,
    onSelect,
    scrollTopRow,
    rowsToDisplay,
  }, ref) => {
  const [selectedRows, setSelectedRows] = useState([]);

  const observer = useRef(null);
  const lastRowElementRef = useCallback(node => {
    if (loading) return;
    if (observer.current) observer.current.disconnect();
    observer.current = new IntersectionObserver(entries => {
      if (entries[0].isIntersecting && hasMore) {
        onPageChange(currentPage + 1);
      }
    });
    if (node) observer.current.observe(node);
  }, [loading, hasMore]);

  useEffect(() => {
    if (selectType === TABLE_SELECT_TYPES.NONE) {
      setSelectedRows([]);
    } else if (selectType === TABLE_SELECT_TYPES.ALL) {
      setSelectedRows(map(rows, row => row.id));
    }
  }, [selectType]);

  useEffect(() => {
    if (typeof onSelect === "function") {
      onSelect(selectedRows);
    }
  }, [selectedRows]);

  const onClickRow = (rowId) => {
    if (multiSelect) {
      handleMultiSelect(rowId);
    } else {
      handleSingleSelect(rowId);
    }
  }

  const handleMultiSelect = (rowId) => {
    onChangeSelectType(TABLE_SELECT_TYPES.PARTIAL);
    if (indexOf(selectedRows, rowId) >= 0) {
      const rows = [...selectedRows];
      pull(rows, rowId);
      setSelectedRows(rows);
      return;
    }

    setSelectedRows([...selectedRows, rowId]);
  }

  const handleSingleSelect = (rowId) => {
    onChangeSelectType(TABLE_SELECT_TYPES.PARTIAL);
    if (selectedRows.length > 0 && selectedRows[0] === rowId) {
      setSelectedRows([]);
      return;
    }
    setSelectedRows([rowId]);
  }

  const getRows = () => {
    if (!columns || !columns.length || !rows || !rows.length) {
      return [];
    }

    const outputRows = [];
    let rowIndex = scrollTopRow || 0;

    for (let i = scrollTopRow; i < rows.length; i++) {
      const row = rows[i];
      if (!row || (row.hasOwnProperty('visible') && !row.visible)) {
        return null;
      }

      rowIndex += 1;
      if (!row.id) {
        row.id = rowIndex;
      }

      const selected = indexOf(selectedRows, row.id) >= 0;
      let refToLastRow = null;
      if (rows.length === i + 1) {
        refToLastRow = lastRowElementRef;
      }
      outputRows.push(<DxTableRow
        key={`table-row-${i}`}
        ref={refToLastRow}
        columns={columns}
        row={row}
        rowIndex={rowIndex}
        onClick={onClickRow}
        onDoubleClick={onDoubleClickRow}
        selected={selected}
      />);

      if (rowIndex === scrollTopRow + rowsToDisplay) {
        break;
      }
    }

    return outputRows;
  }

  return (
    <tbody ref={ref}>
    {getRows()}
    </tbody>
  );
})

export default DxTableBody;
