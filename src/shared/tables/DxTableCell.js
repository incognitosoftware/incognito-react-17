import { inputDataTypes } from '../types/dataTypes';

const DxTableCell = ({ column, content, className }) => {
  if (!column || content === null || content === undefined) {
    return null;
  }

  if (column.dataType.type === inputDataTypes.TEXT) {
    return (
      <td
        className={className}
        title={content}
        style={{
          textAlign: column.align,
          width: column.width,
          verticalAlign: 'middle',
          fontSize: '14px',
          ...column.style
        }}
        dangerouslySetInnerHTML={{ __html: content }}
      />
    );
  }

  return (
    <td
      className={className}
      title={content}
      style={{
        textAlign: column.align,
        verticalAlign: 'middle',
        fontSize: '14px',
        width: column.width,
        ...column.style
      }}
    >
      {content}
    </td>
  );
}

export default DxTableCell;
