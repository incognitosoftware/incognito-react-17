import { useState } from "react";
import { inputDataTypes, typeOfData } from '../types/dataTypes';
import { FormControl, Form } from 'react-bootstrap';
import { translateText } from "../utils/TranslateUtils";

const DxTableFilter = ({ column, onSearch, value }) => {

  const [searchValue, setSearchValue] = useState(value);

  const handleEnter = (event) => {
    if (event.key === 'Enter') {
      onSearchValue(searchValue);
    }
  };

  const onSearchValue = (value) => {
    if (typeof onSearch === typeOfData.FUNCTION) {
      onSearch(column.dataType.type, column.searchKey, value, column.searchByRegex);
    }
  }

  const renderOptions = (options) => {
    if (!options || !options.length) {
      return [];
    }

    const output = [];
    options.forEach(option => {
      output.push(
        <option
          value={option.systemName}
        >
          {
            option.hasOwnProperty('icon') ? translateText(option.systemName) :
              translateText(option.friendlyName)
          }
        </option>
      );
    });
    return output;
  }

  if (!column || !column.searchKey) {
    return null;
  }

  if (column.dataType.type === inputDataTypes.TEXT ||
    column.dataType.type === inputDataTypes.STATICTEXT) {
    return (
      <div style={{ width: '100%', position: 'relative' }}>
        <FormControl
          id={column.searchKey}
          type='text'
          onChange={(event => {
            setSearchValue(event.target.value);
          })}
          onKeyDown={handleEnter}
          className={'incognito-table-filter'}
        />
      </div>
    );
  }

  if (column.dataType.type === inputDataTypes.ENUM ||
    column.dataType.type === inputDataTypes.ICON) {
    //todo: set the value using original state
    return (
      <div style={{ width: '100%', position: 'relative' }}>
        <Form.Select
          id={column.searchKey}
          className={'incognito-table-filter'}
          onChange={(event) => {
            setSearchValue(event.target.value);
            onSearchValue(event.target.value);
          }}
        >
          {
            renderOptions(column.dataType.choice)
          }
        </Form.Select>
      </div>
    );
  }

  if (column.dataType.type === inputDataTypes.DATE) {
    //todo: set the value using original state
    return (
      <div className={'incognito-table-filter'} style={{ width: '100%', position: 'relative' }}>
        <FormControl
          id={column.searchKey}
          type='date'
          onChange={(event) => {
            console.log(event)
          }}
          className={'incognito-table-filter'}
        />
      </div>
    );
  }

  return null;
}

export default DxTableFilter;
