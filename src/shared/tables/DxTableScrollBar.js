import React, { useEffect, useRef, useState } from 'react';

const DxTableScrollBar = ({
                            visible,
                            scrollTrackHeight,
                            scrollTrackTop,
                            scrollPillHeight,
                            scrollPillTop,
                            onScroll
                          }) => {
  const [isDragging, setIsDragging] = useState(false);
  const isDraggingRef = useRef(isDragging);
  isDraggingRef.current = isDragging;

  useEffect(() => {
    window.addEventListener('mousemove', onMouseMove);
    window.addEventListener('mouseup', onMouseUp);

    return () => {
      window.removeEventListener('mousemove', onMouseMove);
      window.removeEventListener('mouseup', onMouseUp);
    };
  }, []);

  const onMouseMove = (e) => {
    if (isDraggingRef && isDraggingRef.current) {
      onScroll(e, false);
    }
  }

  const onMouseUp = () => {
    if (isDraggingRef && isDraggingRef.current) {
      setIsDragging(false);
      onScroll({}, true);
    }
  }

  if (!visible) {
    return null;
  }

  return (
    <div
      className='scroll-table-fake-scroll'
      style={{ height: `${scrollTrackHeight}px`, top: `${scrollTrackTop}px` }}
    >
      <div
        className='scroll-table-fake-scroll-pill'
        style={{ height: `${scrollPillHeight}px`, top: `${scrollPillTop}px` }}
        onMouseDown={() => {
          setIsDragging(true);
        }}
      />
    </div>
  );
};

export default DxTableScrollBar;
