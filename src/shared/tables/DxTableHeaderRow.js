import DxTableFilter from "./DxTableFilter";
import { useTranslation } from "react-i18next";
import { forwardRef } from "react";

const DxTableHeaderRow = forwardRef(({ columns = [], onSearch, filterValues = [] }, ref) => {
  const { t } = useTranslation();

  const getColumnLabels = () => {
    const labels = [];

    if (!columns || !columns.length) {
      return [];
    }

    // todo: sort is pending
    columns.forEach((column, index) => {
      labels.push(
        <th key={`table-head-${index}`} style={{ textAlign: column.align, width: column.width }}>
          {t(column.friendlyName) || ''}
        </th>
      )
    });
    return labels;
  }

  const getColumnFilters = () => {
    const filters = [];
    columns.forEach((column, index) => {
      if (column.hasOwnProperty('searchLoc') && column.searchLoc !== 'table') {
        return;
      }

      let value = '';
      if (filterValues[column.searchKey] && filterValues[column.searchKey].value) {
        value = filterValues[column.searchKey].value;
      }

      filters.push((
        <th key={`table-filter-${index}`}>
          <DxTableFilter column={column} onSearch={onSearch} value={value}/>
        </th>
      ));
    });
    return filters;
  }

  return (
    <thead ref={ref}>
    <tr className={'incognito-table-label-row'}>
      {getColumnLabels()}
    </tr>
    <tr className={'incognito-table-filter-row'}>
      {getColumnFilters()}
    </tr>
    </thead>
  );
})

export default DxTableHeaderRow;
