import { useEffect, useRef, useState } from "react";
import Table from 'react-bootstrap/Table';
import DxTableHeaderRow from './DxTableHeaderRow';
import DxTableBody from "./DxTableBody";
import { TABLE_SELECT_TYPES } from "../../constants/enums";
import useVirtualizedScroll from "../hooks/useVirtualizedScroll";
import { filter, some } from "lodash";
import DxTableScrollBar from "./DxTableScrollBar";

const DxTableBuilder = (
  {
    columns,
    rows,
    width,
    height = '100%',
    className = '',
    multiSelect = false,
    selectType = TABLE_SELECT_TYPES.NONE,
    onChangeSelectType,
    onPageChange,
    totalRecordsCount,
    onSearch,
    filters,
    loading,
    onDoubleClickRow,
    onSelectRows,
    layoutHeight,
    enableVirtualization
  }) => {

  // todo: expandable rows support needs to be implemented
  const [currentPage, setCurrentPage] = useState(0);
  const headerRef = useRef(null);
  const bodyRef = useRef(null);
  const [rowHeight, setRowHeight] = useState(1);
  const [headerHeight, setHeaderHeight] = useState(1);
  const [columnsState, setColumnsState] = useState(columns);

  const onPageChangeLocal = (pageNumber) => {
    onPageChange(pageNumber);
    setCurrentPage(pageNumber);
  }

  useEffect(() => {
    if (enableVirtualization) {
      const element = document.querySelector('.incognito-page-container');
      element.style.overflowY = 'hidden';
    }
  }, []);

  useEffect(() => {
    const visibleColumns = filter(columns, column => {
      if (column.hasOwnProperty('visible')) {
        return column.visible;
      }
      return true;
    });
    setColumnsState(visibleColumns);
  }, [columns])

  useEffect(() => {
    if (headerRef && headerRef.current) {
      setHeaderHeight(headerRef.current.clientHeight);
    }

    if (bodyRef && bodyRef.current && bodyRef.current.firstChild) {
      setRowHeight(bodyRef.current.firstChild.clientHeight);
    }
  }, [rows]);

  useEffect(() => {
    setCurrentPage(0);
  }, [filters]);

  const { rowsToDisplay, scrollTopRow, scrollTrackTop, scrollTrackHeight, scrollPillTop, scrollPillHeight, onScroll } =
    useVirtualizedScroll({
      hasTableFilters: false,
      hasHeaderFilters: some(columnsState, (column) => column.hasOwnProperty('searchKey')),
      totalResultLength: rows.length,
      parentHeight: layoutHeight || 0,
      headerHeight,
      rowHeight
    });

  return (
    <div style={{ height: height, display: 'flex', flexDirection: 'row' }}>
      <Table id={className} className={`incognito-table ${className}`} style={{ width: width, float: 'left' }}>
        <DxTableHeaderRow
          ref={headerRef}
          columns={columnsState}
          onSearch={onSearch}
          filterValues={filters}
        />
        <DxTableBody
          ref={bodyRef}
          columns={columnsState}
          rows={rows}
          className={className}
          multiSelect={multiSelect}
          selectType={selectType}
          onChangeSelectType={onChangeSelectType}
          currentPage={currentPage}
          onPageChange={onPageChangeLocal}
          loading={loading}
          hasMore={rows.length < totalRecordsCount}
          onDoubleClickRow={onDoubleClickRow}
          onSelect={onSelectRows}
          scrollTopRow={enableVirtualization ? scrollTopRow : 0}
          rowsToDisplay={enableVirtualization ? rowsToDisplay : rows.length}
        />
      </Table>
      {
        enableVirtualization &&
        <DxTableScrollBar
          scrollPillHeight={scrollPillHeight}
          scrollPillTop={scrollPillTop}
          scrollTrackHeight={scrollTrackHeight}
          scrollTrackTop={scrollTrackTop}
          visible={rows && rows.length}
          onScroll={onScroll}
        />
      }
    </div>
  );
}

export default DxTableBuilder;
