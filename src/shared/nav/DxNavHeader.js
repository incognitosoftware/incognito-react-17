import React, { useEffect, useState } from 'react'
import { Link } from "react-router-dom"
import Nav from 'react-bootstrap/Nav'
import Navbar from 'react-bootstrap/Navbar'
import NavDropdown from 'react-bootstrap/NavDropdown'
import Container from 'react-bootstrap/Container'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCog, faHome, faArrowAltCircleLeft } from '@fortawesome/free-solid-svg-icons'
import { useTranslation } from "react-i18next";

const langs = {
  'en': 'English',
  'fr': 'French',
  'es': 'Spanish',
  'ch': 'Chinese'
}

const NavHeader = (props) => {
  const [language, setLanguage] = useState(langs[props.language]);
  const [expanded, setExpanded] = useState(false);
  const { i18n } = useTranslation();

  useEffect(() => {
    i18n.changeLanguage(props.language);
  }, []);

  const changeLanguage = (lang) => {
    setLanguage(langs[lang]);
    i18n.changeLanguage(lang);
  }

  const handleClick = () => {
    props.history.goBack();
  }

  return (
    <Navbar expanded={expanded} variant="dark" expand="sm" fixed="top" className="incognito-nav-color">
      <Container>
        {props.token ?
          <Navbar.Brand>
            <Link className="navbar-brand" to="/dx/hub"><FontAwesomeIcon icon={faHome} /></Link>
            <Link className="navbar-brand" to={""} onClick={handleClick}><FontAwesomeIcon icon={faArrowAltCircleLeft} /></Link>
          </Navbar.Brand>
        : null}
        <Navbar.Toggle aria-controls="responsive-navbar-nav" onClick={() => setExpanded(expanded ? false : "expanded")}/>
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="me-auto"></Nav>
          <Nav>
            {props.token ?
              <NavDropdown title={<span><FontAwesomeIcon icon={faCog}/></span>} id="basic-nav-dropdown">
                <NavDropdown.Item href="">Action</NavDropdown.Item>
                <NavDropdown.Item href="">Another action</NavDropdown.Item>
                <NavDropdown.Item href="">Something</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item onClick={props.forceNoAuthLogout}>Logout</NavDropdown.Item>
              </NavDropdown> :
              <NavDropdown title={language} id="basic-nav-dropdown">
                <NavDropdown.Item onClick={() => { changeLanguage('en') }} href="">English</NavDropdown.Item>
                <NavDropdown.Item onClick={() => { changeLanguage('fr') }} href="">French</NavDropdown.Item>
                <NavDropdown.Item onClick={() => { changeLanguage('es') }} href="">Spanish</NavDropdown.Item>
                <NavDropdown.Item onClick={() => { changeLanguage('ch') }} href="">Chinese</NavDropdown.Item>
              </NavDropdown>
            }
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default NavHeader;
