import { Route, Redirect } from 'react-router-dom';

export default function DxPrivateRoute ({component: Component, token, ...rest}) {

  return (
    <Route
      {...rest}
      render={(props) => token
        ? <Component {...rest} />
        : <Redirect to={{pathname: '/dx'}} />}
    />
  )
}