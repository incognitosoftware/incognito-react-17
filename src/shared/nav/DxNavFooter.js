import Nav from 'react-bootstrap/Nav'
import Navbar from 'react-bootstrap/Navbar'
import Container from 'react-bootstrap/Container'

const NavFooter = (props) => {
  return (
    <Navbar variant="dark" expand="sm" fixed="bottom" className="incognito-nav-color">
      <Container>
        <Nav> 
          <Navbar.Text>Copyright ({new Date().getFullYear()})</Navbar.Text>
        </Nav>
        <Nav> 
          <Nav.Link target="_blank" href="http://www.incognito.com">incognito.com</Nav.Link>
        </Nav>  
      </Container>
    </Navbar>
  );
}

export default NavFooter;
