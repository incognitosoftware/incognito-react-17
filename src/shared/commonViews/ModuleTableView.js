import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { find } from 'lodash';
import Container from 'react-bootstrap/Container';
import DxTableBuilder from "../tables/DxTableBuilder";
import DxSpinner from "../common/DxSpinner";
import { fetchData } from "../data/DataApi";
import DxModuleHeader from "../common/DxModuleHeader";
import DxModuleSubHeader from "../common/DxModuleSubHeader";
import { TABLE_SELECT_TYPES } from '../../constants/enums';
import { DEFAULT_PAGE_SIZE } from "../../constants/common";
import { columnDataTypes } from "../types/dataTypes";
import useLayoutHeight from "../hooks/useLayoutHeight";

const ModuleTableView = (
  {
    history, tableJson, url, formatResponseData, headerButtons, module, title, enableDoubleClick,
    navigateOnOneMatch, query
  }) => {
  const [loading, setLoading] = useState(false);
  const [totalRecords, setTotalRecords] = useState(0);
  const [data, setData] = useState([]);
  const [selectState, setSelectState] = useState(TABLE_SELECT_TYPES.NONE);
  const [filter, setFilter] = useState({});
  const { layoutHeight, layoutRef } = useLayoutHeight(data, 25);

  const { t } = useTranslation();

  useEffect(() => {
    setData([]);
    setTotalRecords(0);

    if (!loading) loadData(0);
  }, [filter]);

  const buildSearchQuery = () => {
    let filterQuery = '&q=';
    Object.keys(filter).forEach(key => {
      const filterObject = filter[key];
      if (filterObject && filterObject.value && filterObject.searchKey) {
        if (filterObject.columnDataType === columnDataTypes.ENUM && filterObject.value === 'ALL') {
          return;
        }

        const regex = filterObject.searchByRegex ? '*' : '';
        if (filterQuery === '&q=') {
          filterQuery = `${filterQuery}${filterObject.searchKey}%3D${regex}${filterObject.value}${regex}`;
          return;
        }
        filterQuery = `${filterQuery} AND ${filterObject.searchKey}%3D${regex}${filterObject.value}${regex}`;
      }
    })
    return filterQuery === '&q=' ? '' : filterQuery;
  };

  const loadData = async (currentPage) => {
    setLoading(true);
    try {
      let searchQuery = buildSearchQuery();
      if (!searchQuery && query) {
        searchQuery = `&q=${query}`
      }

      const URL = `${url}?pageSize=${DEFAULT_PAGE_SIZE}&pageIndex=${currentPage}${searchQuery}`;
      const dataResp = await fetchData(URL);
      setLoading(false);

      if (dataResp.error) {
        // todo: show error
        return;
      }

      let formattedData = dataResp.data.results;
      if (typeof formatResponseData === "function") {
        formattedData = formatResponseData(dataResp.data.results);
      }

      if (navigateOnOneMatch && formattedData.length === 1) {
        onDoubleClickRow(formattedData[0].identifier, true);
      }

      setData(data => [...data, ...formattedData]);
      setTotalRecords(dataResp.data.totalRecordCount);
    } catch (e) {
      setLoading(false);
      // todo: show error
    }
  }

  const onSelectAll = () => {
    setSelectState(TABLE_SELECT_TYPES.ALL);
  }

  const onSelectNone = () => {
    setSelectState(TABLE_SELECT_TYPES.NONE);
  }

  const onChangeSelectState = (state) => {
    setSelectState(state);
  }

  const onPageChange = async (currentPage) => {
    await loadData(currentPage);
  }

  const onSearch = async (columnDataType, searchKey, value, searchByRegex = false) => {
    setFilter(Object.assign({}, filter,
      { [searchKey]: { searchKey, columnDataType, value, searchByRegex } }))
  }

  const onDoubleClickRow = (rowId, isIdentifier = false) => {
    if (!enableDoubleClick || !module) {
      return;
    }

    if (isIdentifier) {
      history.push(`/dx/${module}/${rowId}`);
      return;
    }

    const entity = find(data, { id: rowId });
    history.push(`/dx/${module}/${entity.identifier}`);
  }

  return (
    <div className="incognito-route-container">
      <DxModuleHeader title={t(title)}/>
      <DxModuleSubHeader
        buttons={headerButtons}
        totalCount={totalRecords}
        showingCount={data ? data.length : 0}
        onSelectAll={onSelectAll}
        onSelectNone={onSelectNone}
      />

      <Container className="incognito-page-container top-spacing" ref={layoutRef}>
        <DxSpinner visible={loading}/>
        <DxTableBuilder
          columns={tableJson}
          rows={data}
          height={'100%'}
          multiSelect={true}
          selectType={selectState}
          onChangeSelectType={onChangeSelectState}
          totalRecordsCount={totalRecords}
          pageSize={DEFAULT_PAGE_SIZE}
          paginationEnabled
          onPageChange={onPageChange}
          onSearch={onSearch}
          filters={filter}
          loading={loading}
          onDoubleClickRow={onDoubleClickRow}
          layoutHeight={layoutHeight}
          enableVirtualization
        />
      </Container>

    </div>
  );

}

export default ModuleTableView;
