import { findIndex } from 'lodash';

export const findIndexByName = (collection = [], searchKey, searchValue) => {
  return findIndex(collection, { [searchKey]: searchValue });
}
