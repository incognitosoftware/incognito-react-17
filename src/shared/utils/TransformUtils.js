import { omit } from 'lodash';

export const getDevicesMapFromAccounts = (accounts) => {
  if (!accounts || !accounts.results || !accounts.results.length) {
    return {};
  }

  const devices = {};
  accounts.results.forEach(account => {
    if (!account.subscribers || !account.subscribers.length) {
      return;
    }

    const currentAccount = omit(account, ['subscribers', 'identifier']);
    account.subscribers.forEach(subscriber => {
      if (!subscriber.devices || !subscriber.devices.length) {
        return;
      }

      const currentSubscriber = omit(subscriber, ['services', 'devices']);
      subscriber.devices.forEach(device => {
        const selectedDevice = devices[device.identifier] ? devices[device.identifier] : device;
        if (selectedDevice.hasOwnProperty('accounts')) {
          selectedDevice.accounts.push(currentAccount);
        } else {
          selectedDevice.accounts = [currentAccount];
        }

        if (selectedDevice.hasOwnProperty('subscribers')) {
          selectedDevice.subscribers.push(currentSubscriber);
        } else {
          selectedDevice.subscribers = [currentSubscriber];
        }
        devices[device.identifier] = selectedDevice;
      });
    });
  });
  return devices;
}
