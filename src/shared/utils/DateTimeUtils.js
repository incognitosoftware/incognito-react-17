import dayjs from "dayjs";
import { translateText } from "./TranslateUtils";

export const getFormattedDate = (dateStr) => {
  if (!dateStr) {
    return translateText('STATUS_NO_DATA');
  }
  return dayjs(new Date(dateStr)).format('DD MMM YYYY');
}

export const getFormattedTime = (timeStr) => {
  if (!timeStr) {
    return translateText('STATUS_NO_DATA');
  }
  return dayjs(new Date(timeStr)).format('HH:mm:ss');
}

export const getDigitalTime = (seconds) => {
  const newDate = (new Date(seconds * 1000)).toUTCString().match(/(\d\d:\d\d:\d\d)/);
  if (!newDate) {
    return '';
  }
  return newDate[0];
}
