import { useState } from 'react';

const useForm = () => {

  const [values, setValues] = useState({});

  const handleChange = (e) => {
    if (e) { 
      e.persist();
    }

    setValues(values => ({ ...values, [e.target.id]: e.target.value }));
  };

  return {
    handleChange,
    values,
  }
};

export default useForm;