import React, { useState } from 'react'
import Form from 'react-bootstrap/Form'
import FormTitle from './DxFormTitle'
import FormInput from './DxFormInput'
import FormButton from './DxFormButton'
import useForm from "./useForm";
import FormStaticText from "./DxFormStaticText";
import DxFormSelect from "./DxFormSelect";
import DxFormTextArea from "./DxFormTextArea";

const DxFormBuilder = (props) => {
  const { values, handleChange } = useForm();
  const [validated, setValidated] = useState(false);
  const [loading, setLoading] = useState(false);

  const createForm = () => {
    const output = [];

    props.fields.forEach((field, idx) => {

      switch (field.dataType.type) {
        case 'TITLE':
          output.push(<FormTitle {...props} key={idx} field={field}/>);
          break;
        case 'BUTTON':
          output.push(<FormButton {...props} key={idx} field={field} loading={loading}/>);
          break;
        case 'STATIC_TEXT':
          output.push(<FormStaticText {...props} key={idx} field={field}/>);
          break;
        case 'SELECT':
          output.push(<DxFormSelect {...props} key={idx} field={field}/>);
          break;
        case 'TEXT_AREA':
          output.push(<DxFormTextArea {...props} key={idx} field={field}/>);
          break;
        default:
          output.push(<FormInput {...props} key={idx} field={field} onChange={handleChange}/>);
          break;
      }
    });

    return <Form noValidate validated={validated} onSubmit={onSubmit}>{output}</Form>;
  }

  const onSubmit = (e) => {
    const form = e.currentTarget;
    const canSubmit = form.checkValidity();

    e.preventDefault();
    e.stopPropagation();

    setLoading(true);

    if (canSubmit) {
      props.submitFunc(values, updateForm);
    } else {
      setLoading(false);
    }

    setValidated(true);
  }

  const updateForm = (fields) => {
    if (fields && !fields.length) {
      document.querySelector('form').reset();
    } else {
      //todo - clear only certain fields
    }

    setLoading(false);
  }

  return (
    createForm()
  );
};

export default DxFormBuilder;
