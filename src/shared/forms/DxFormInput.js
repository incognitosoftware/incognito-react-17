import React from 'react'
import Form from 'react-bootstrap/Form'
import { useTranslation } from "react-i18next";
import { Col, Row } from "react-bootstrap";

const FormInput = (props) => {
  const { t } = useTranslation();
  return (
    <Form.Group as={Row} order={props.field.order} controlId={props.field.systemName} className="mb-3">
      <Col sm={3}>
        <Form.Label className="incognito-form-label">{t(props.field.friendlyName)}</Form.Label>
      </Col>
      <Col sm={9}>
        <Form.Control required={props.field.required} type={props.field.dataType.type} onChange={props.onChange}/>
        <Form.Control.Feedback type="invalid">
          {props.field.errorMsg}
        </Form.Control.Feedback>
        {props.field.helpMsg ? <Form.Text className="text-muted">{props.field.helpMsg}</Form.Text> : null}
      </Col>
    </Form.Group>
  );

}

export default FormInput;
