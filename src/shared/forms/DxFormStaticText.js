import React from 'react';
import { useTranslation } from "react-i18next";
import { Col, Form, Row } from "react-bootstrap";

const DxFormStaticText = (props) => {
  const { t } = useTranslation();
  return (
    <Form.Group as={Row} order={props.field.order} controlId={props.field.systemName} className="mb-3">
      <Col sm={3}>
        <Form.Label column className="incognito-form-label">
          {t(props.field.friendlyName)}
        </Form.Label>
      </Col>
      <Col sm={9}>
        <Form.Control className="incognito-form-text" plaintext readOnly defaultValue="-"
                      value={props.field.value ? props.field.value : '-'}/>
      </Col>
    </Form.Group>
  );
};

export default DxFormStaticText;
