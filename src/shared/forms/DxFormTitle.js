import React from 'react'
import Form from 'react-bootstrap/Form'

const FormTitle = (props) => {

  return (
    <Form.Group order={props.field.order} controlId={props.field.systemName} className="mb-3">
      <Form.Label className="incognito-form-title">{props.field.friendlyName}</Form.Label>
    </Form.Group>
  );

}

export default FormTitle;
