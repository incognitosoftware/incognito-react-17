import React   from 'react'
import Form    from 'react-bootstrap/Form'
import Button  from 'react-bootstrap/Button'
import Spinner from 'react-bootstrap/Spinner'

const FormButton = (props) => {

  return (
    <Form.Group order={props.field.order} controlId={props.field.systemName}>
      <Button type={props.field.dataType.submit ? "submit" : ""} className="incognito-btn" style={props.field.style || {}}>
        {props.loading ? <Spinner animation="border" size="sm" /> : null}
        {props.field.friendlyName}
      </Button>
    </Form.Group>
  );

}

export default FormButton;
