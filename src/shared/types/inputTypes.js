export const onBlurInputs = [
  'TEXT',
  'NUMBER',
  'PASSWORD'
];

export const onChangeInputs = [
  'ENUM',
  'CHECKBOX',
  'RADIO'
];
