import { BASE_URL, URL_AUTHENTICATION } from '../../constants/urls';

export const authenticate = async (headers, body) => {
  const resp = await fetch(`${BASE_URL}/${URL_AUTHENTICATION}`, {
    method: 'POST',
    headers: headers,
    body: JSON.stringify(body)
  });
  const data = await resp;
  const json = await resp.json();

  if (data.status === 201) {
    return { data: json, error: false };
  } else {
    return { data: json, error: true };
  }
}

export const fetchData = async (uri, headers = {}) => {
  const headersWithToken = {
    authorization: sessionStorage.getItem('token'),
    ...headers
  }
  const resp = await fetch(`${BASE_URL}/${uri}`, {
    method: 'GET',
    headers: headersWithToken
  });

  const data = await resp;
  const json = await resp.json();

  if (data.status === 200) {
    return { data: json, error: false };
  } else {
    return { data: json, error: true };
  }
}
