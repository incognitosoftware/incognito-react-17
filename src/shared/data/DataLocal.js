export const getLocalJson = async (url, headers) => {
  const resp = await fetch(url, {
    headers: headers
  });
  
  const json = await resp.text();
  
  return json;
}

