import React, { useEffect, useRef, useState } from 'react';

const useVirtualizedScroll = ({
                                parentHeight,
                                totalResultLength,
                                headerHeight,
                                rowHeight
                              }) => {
  const [scrollTopRow, setScrollTopRow] = useState(0);
  const [scrollPillTop, setScrollPillTop] = useState(0);
  const [scrollPillHeight, setScrollPillHeight] = useState(0);
  const [scrollTrackHeight, setScrollTrackHeight] = useState(0);
  const [scrollTrackTop, setScrollTrackTop] = useState(0);
  const [rowsToDisplay, setRowsToDisplay] = useState(0);
  const stateRef = useRef({});
  stateRef.current = {
    scrollTopRow: scrollTopRow,
    scrollPillHeight: scrollPillHeight,
    scrollPillTop: scrollPillTop,
    scrollTrackHeight: scrollTrackHeight,
    scrollTrackTop: scrollTrackTop,
    rowsToDisplay: rowsToDisplay,
    totalResultLength: totalResultLength
  };
  const oldCursorRef = useRef(0);

  useEffect(() => {
    if (parentHeight > 0) {
      calculateScrollData();
      window.removeEventListener('wheel', addWheelListener);
      window.addEventListener('wheel', addWheelListener);
    }
  }, [parentHeight]);

  useEffect(() => {
    updateOnRowsChange();
  }, [totalResultLength]);

  const updateOnRowsChange = () => {
    let scrollPillHeightTemp = Math.ceil(scrollTrackHeight * (rowsToDisplay / stateRef.current.totalResultLength));
    let scrollPillTopTemp = (scrollTopRow / stateRef.current.totalResultLength) * scrollTrackHeight;

    if (scrollPillHeightTemp > scrollTrackHeight) {
      scrollPillHeightTemp = scrollTrackHeight;
    }

    if (scrollPillTopTemp < 0) {
      scrollPillTopTemp = 0;
    } else if (scrollPillTopTemp > scrollTrackHeight - scrollPillHeightTemp) {
      scrollPillTopTemp = scrollTrackHeight - scrollPillHeightTemp;
    }
    setScrollPillTop(scrollPillTopTemp);
    setScrollPillHeight(scrollPillHeightTemp);
  }

  const calculateScrollData = () => {
    const scrollTrackHeightTemp = parentHeight - headerHeight
    const rowsToDisplayTemp = Math.ceil(scrollTrackHeightTemp / rowHeight);
    let scrollPillHeightTemp = Math.ceil(scrollTrackHeightTemp * (rowsToDisplayTemp / stateRef.current.totalResultLength));

    if (scrollPillHeightTemp > scrollTrackHeightTemp) {
      scrollPillHeightTemp = scrollTrackHeightTemp;
    }

    setRowsToDisplay(rowsToDisplayTemp);
    setScrollPillHeight(scrollPillHeightTemp);
    setScrollTrackHeight(scrollTrackHeightTemp)
    setScrollTrackTop(headerHeight)
  };

  const addWheelListener = (e) => {
    const current = stateRef.current;

    let scrollTopRowTemp = current.scrollTopRow + Math.floor(e.deltaY / rowHeight);
    let scrollPillTopTemp = (scrollTopRowTemp / stateRef.current.totalResultLength) * current.scrollTrackHeight;
    updateOnScroll(scrollPillTopTemp, scrollTopRowTemp);
  }

  const onScroll = (e, isCompleted) => {
    if (isCompleted) {
      oldCursorRef.current = 0;
      return;
    }
    const current = stateRef.current;

    if (oldCursorRef.current === 0) {
      oldCursorRef.current = e.clientY;
    }

    const diff = e.clientY - oldCursorRef.current;
    if (diff === 0 || Math.abs(diff) < Math.abs(current.scrollPillHeight / 2)) {
      return;
    }
    let scrollTopRowTemp;
    if (diff >= 0) {
      scrollTopRowTemp = current.scrollTopRow +
        Math.floor(stateRef.current.totalResultLength *
          ((e.clientY - oldCursorRef.current) / current.scrollTrackHeight));
    } else {
      scrollTopRowTemp = current.scrollTopRow +
        Math.ceil(stateRef.current.totalResultLength *
          ((e.clientY - oldCursorRef.current) / current.scrollTrackHeight));
    }
    const scrollPillTopTemp = Math.floor((scrollTopRowTemp / stateRef.current.totalResultLength) * current.scrollTrackHeight);

    oldCursorRef.current = e.clientY;
    updateOnScroll(scrollPillTopTemp, scrollTopRowTemp);
  }

  const updateOnScroll = (scrollPillTopValue, scrollTopRowValue) => {
    const current = stateRef.current;
    let scrollPillTopTemp = scrollPillTopValue;
    let scrollTopRowTemp = scrollTopRowValue;

    if (scrollPillTopTemp < 0) {
      scrollPillTopTemp = 0;
    } else if (scrollPillTopTemp > current.scrollTrackHeight - current.scrollPillHeight) {
      scrollPillTopTemp = current.scrollTrackHeight - current.scrollPillHeight;
    }

    if (scrollTopRowTemp < 0) {
      scrollTopRowTemp = 0;
    } else if (scrollTopRowTemp > stateRef.current.totalResultLength - current.rowsToDisplay + 1) {
      // adding one because we used ceil function to populate the rowsToDisplay
      scrollTopRowTemp = current.scrollTopRow;
    }

    setScrollTopRow(scrollTopRowTemp);
    setScrollPillTop(scrollPillTopTemp);
  }

  return { scrollTopRow, scrollPillHeight, scrollPillTop, scrollTrackHeight, scrollTrackTop, rowsToDisplay, onScroll };
};

export default useVirtualizedScroll;
