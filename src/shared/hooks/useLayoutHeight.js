import React, { useEffect, useRef, useState } from 'react';

const useLayoutHeight = (triggerProp, totalPadding = 0) => {
  const layoutRef = useRef(null);

  const [layoutHeight, setLayoutHeight] = useState(0);
  useEffect(() => {
    if (layoutRef.current) {
      setLayoutHeight(layoutRef.current.offsetHeight - totalPadding);
    }
  }, [triggerProp]);

  return {layoutRef, layoutHeight};
};

export default useLayoutHeight;
