import React from 'react';
import Container from "react-bootstrap/Container";
import DxSearchBox from "./DxSearchBox";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faRotate } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";
import ActionDropdown from "../dropdowns/ActionDropdown";

function DxEntityHeader({ module, title, onSearch, onRefresh, searchPlaceholder, actions }) {
  return (
    <Container>
      <div className="incognito-app-entity-header">
        <div className="incognito-entity-module-title">{module}</div>
        <div className={"incognito-title-container"}>
          <div className="incognito-entity-title">{title}</div>
          <div className="incognito-app-entity-header-right">
            {
              typeof onSearch === "function" &&
              <DxSearchBox onSearch={onSearch} placeholder={searchPlaceholder}/>
            }
            {
              typeof onRefresh === "function" &&
              <Link
                className="incognito-app-entity-header-right-refresh"
                onClick={onRefresh}
              >
                <FontAwesomeIcon size='xl' icon={faRotate}/>
              </Link>
            }
            {
              actions && actions.length &&
              <div className="incognito-app-entity-header-right-actions">
                <ActionDropdown actions={actions}/>
              </div>
            }
          </div>
        </div>
      </div>
    </Container>
  );
}

export default DxEntityHeader;
