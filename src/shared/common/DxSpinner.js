import Spinner from "react-bootstrap/Spinner";

const DxSpinner = ({ visible = false }) => {
  if (!visible) {
    return null;
  }

  return (
    <div className={'incognito-spinner-container'}>
      <Spinner className={'incognito-spinner'} animation={'border'} variant='primary'/>
    </div>
  );
}

export default DxSpinner;
