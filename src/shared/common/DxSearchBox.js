import React, { useState } from 'react';
import { Button, Form, FormControl, InputGroup } from "react-bootstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

const DxSearchBox = ({ onSearch, placeholder }) => {
  const [searchString, setSearchString] = useState('');
  return (
    <Form onSubmit={(e) => {
      e.preventDefault();
      typeof onSearch === 'function' && onSearch(searchString);
    }}>
      <InputGroup size="sm" className="mb-3 incognito-search-box">
        <FormControl
          placeholder={placeholder}
          aria-label={placeholder}
          aria-describedby="basic-addon2"
          onChange={(value) => setSearchString(value.target.value)}
        />
        <Button
          variant="info"
          id="button-addon2"
          className="incognito-search-button"
          onClick={() => typeof onSearch === 'function' && onSearch(searchString)}
        >
          <FontAwesomeIcon icon={faSearch}/>
        </Button>
      </InputGroup>
    </Form>
  );
};

export default DxSearchBox;
