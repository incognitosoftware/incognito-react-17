import React from 'react';
import { Container } from "react-bootstrap";

function DxModuleSubHeader(
  {
    totalCount,
    showingCount,
    onSelectAll,
    onSelectNone,
    buttons = []
  }) {
  return (
    <Container>
      <div className={'row justify-content-start'}>
        <div className={'col-2'}>
          {
            (showingCount && totalCount) ?
            <div className={'incognito-sub-header-count'}>
              {`Showing ${showingCount} of ${totalCount}`}
            </div> : null
          }
          <div className={'incognito-sub-header-detail'}>
            <div>Select :</div>
            <button key='all_button' className={'btn btn-link'} onClick={onSelectAll}>All</button>
            <button key='none_button' className={'btn btn-link'} onClick={onSelectNone}>None</button>
          </div>
        </div>
        <div className={'col-10 incognito-sub-header-buttons'}>
          {
            buttons.map((button, index) => (
              <button
                key={`sub-header-button-${index}`}
                className={`btn btn-sm ${button.disabled ? 'btn-secondary' : 'btn-primary'}`}
                disabled={button.disabled}
                title={button.title}
                onClick={() => typeof button.onClick === 'function' && button.onClick()}
              >
                {button.title}
              </button>
            ))
          }
        </div>
      </div>
    </Container>
  );
}

export default DxModuleSubHeader;
