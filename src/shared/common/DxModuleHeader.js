import React from 'react';
import Container from "react-bootstrap/Container";

function DxModuleHeader({title}) {
  return (
    <Container>
      <div className="incognito-app-header">
        <div className="incognito-app-title">{title}</div>
      </div>
    </Container>
  );
}

export default DxModuleHeader;
