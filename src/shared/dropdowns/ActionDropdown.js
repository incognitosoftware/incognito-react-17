import React from 'react';
import { Dropdown } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEllipsisVertical } from "@fortawesome/free-solid-svg-icons";
import { dropdownMenuType } from "../types/dataTypes";

const ActionDropdown = ({ actions, onClickAction }) => {
  if (!actions || !actions.length) {
    return null;
  }

  return (
    <Dropdown id='dropdown'>
      <Dropdown.Toggle id='dropdown-toggle' variant="link" bsPrefix="p-0">
        <FontAwesomeIcon id='dropdown-icon' size="xl" icon={faEllipsisVertical} className="incognito-action-dropdown-toggle"/>
      </Dropdown.Toggle>
      <Dropdown.Menu>
        {
          actions.map(action => {
            if (action.type && action.type === dropdownMenuType.HEADING) {
              return <Dropdown.Header>{action.name}</Dropdown.Header>;
            }
            if (action.type && action.type === dropdownMenuType.DIVIDER) {
              return <Dropdown.Divider/>;
            }

            return (<Dropdown.Item
              onClick={() => {
                if (typeof action.onClick === "function") {
                  action.onClick();
                  return;
                }

                if (typeof onClickAction === "function") {
                  onClickAction(action.name);
                }
              }}
            >
              {action.name}
            </Dropdown.Item>);
          })
        }
      </Dropdown.Menu>
    </Dropdown>
  );
};

export default ActionDropdown;
