import React, { useState } from 'react';
import { Dropdown } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { find } from 'lodash';
import { faEllipsisVertical } from "@fortawesome/free-solid-svg-icons";
import { faCheckSquare, faSquare } from "@fortawesome/free-regular-svg-icons";

const ColumnSelectionDropdown = ({ columns, onColumnVisibilityChange }) => {
  const [columnsState, setColumnState] = useState(columns);

  if (!columns || !columns.length) {
    return null;
  }

  const onColumnClick = (column) => {
    const columnsStateTemp = JSON.parse(JSON.stringify(columnsState));
    const selectedColumn = find(columnsStateTemp, column);
    selectedColumn.visible = !getVisibility(selectedColumn);
    if (typeof onColumnVisibilityChange === 'function') {
      onColumnVisibilityChange(selectedColumn);
    }
    setColumnState(columnsStateTemp);
  };

  const getVisibility = (column) => {
    if (column.hasOwnProperty('visible')) {
      return column.visible;
    }
    return true;
  };

  return (
    <Dropdown autoClose="outside">
      <Dropdown.Toggle variant="link" bsPrefix="p-0">
        <FontAwesomeIcon size="xl" icon={faEllipsisVertical} className="incognito-action-dropdown-toggle"/>
      </Dropdown.Toggle>
      <Dropdown.Menu>
        <Dropdown.Header>Show Columns</Dropdown.Header>
        <Dropdown.Divider/>
        {
          columnsState.map(column => {
            if (!column.friendlyName && !column.name) {
              return null;
            }
            return (<Dropdown.Item
              onClick={() => {
                onColumnClick(column);
              }}
            >
              <div>
                {
                  getVisibility(column) ? <FontAwesomeIcon size="xl" icon={faCheckSquare}/> :
                    <FontAwesomeIcon size="xl" icon={faSquare}/>
                }
                <span style={{ paddingLeft: '10px' }}>{column.friendlyName || column.name}</span>
              </div>
            </Dropdown.Item>);
          })
        }
      </Dropdown.Menu>
    </Dropdown>
  );
};

export default ColumnSelectionDropdown;
