import React from 'react'
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import { modalSize } from "../types/dataTypes";

const DxModalBuilder = (props) => {
  const getButtons = (side) => {
    if (!props.modalConfig[`${side}Buttons`] || !props.modalConfig[`${side}Buttons`].length) {
      return [];
    }

    const buttons = [];
    props.modalConfig[`${side}Buttons`].forEach((button, idx) => {
      buttons.push(
        <Button
          key={`modal-button-${idx}`}
          className={`btn btn-sm ${button.disabled ? 'btn-secondary' : 'btn-primary'}`}
          disabled={button.disabled}
          title={button.title}
          style={{ margin: side === 'left' ? '0 5px 0 0' : '0 0 0 5px' }}
          onClick={() => typeof button.onClick === 'function' && button.onClick()}
          {...button}
        >
          {button.title}
        </Button>
      );
    });

    return buttons;
  }

  return (
    <>
      <Modal size={props.modalConfig.size || modalSize.MEDIUM } backdrop="static" show={props.showModal}>
        <Modal.Header onHide={props.onClose} closeButton style={{ borderBottom: "none" }}>
          <Modal.Title>
            <div className="incognito-modal-sub-title">{props.modalConfig.subTitle}</div>
            <div className="incognito-modal-title">{props.modalConfig.title}</div>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {props.modalConfig.body}
        </Modal.Body>
        <Modal.Footer>
          <div className="incognito-modal-footer">
            <div>
              {getButtons('left')}
            </div>
            <div>
              {getButtons('right')}
            </div>
          </div>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default DxModalBuilder;
