import React, { useEffect, useState } from 'react';
import { typeOfData } from "../types/dataTypes";

let countdownTimer;
const Counter = ({ onCountEnd }) => {
  let [count, setCount] = useState(10);

  useEffect(() => {
    countdownTimer = setInterval(countdown, 1000);
    return () => {
      clearInterval(countdownTimer);
    };
  }, []);

  const countdown = () => {
    if (count > 0) {
      setCount(count -= 1);
      return;
    }
    if (count <= 0) {
      setCount(10);
      if (countdownTimer) clearInterval(countdownTimer);
      countdownTimer = null;
      if (typeof onCountEnd === typeOfData.FUNCTION) onCountEnd();
    }
  }

  return (<p>Session will auto-expire in {count} seconds</p>);
};

export default Counter;
