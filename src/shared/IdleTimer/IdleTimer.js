import React, {
  useEffect,
  useState
} from 'react';
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import Counter from "./Counter";
import { DEFAULT_SESSION_TIMEOUT } from "../../constants/common";

let warnTimeout;

const IdleTimer = (props) => {
  const [warningTime] = useState(DEFAULT_SESSION_TIMEOUT);
  const [showModal, setShowModal] = useState(false);

  useEffect(() => {
    const events = [
      'mousemove',
      'keypress'
    ];

    const resetTimeout = () => {
      clearTimeouts();
      setTimeouts();
    };

    for (let i in events) {
      window.addEventListener(events[i], resetTimeout);
    }
    setTimeouts();

    return () => {
      for (let i in events) {
        window.removeEventListener(events[i], clearTimeoutsOnTermination);
      }
    }
  }, []);

  const warn = () => {
    setShowModal(true);
  };

  const logout = () => {
    setShowModal(false);
    props.forceNoAuthLogout();
  }

  const stayLoggedIn = () => {
    clearTimeouts();
    props.stayLoggedIn();
    setShowModal(false);
  }

  const setTimeouts = () => {
    if (!showModal) {
      warnTimeout = setTimeout(warn, warningTime * 1000);
    }
  };

  const clearTimeouts = () => {
    if (!showModal && warnTimeout) {
      clearTimeout(warnTimeout);
      warnTimeout = null;
    }
  };

  const clearTimeoutsOnTermination = () => {
    if (warnTimeout) {
      clearTimeout(warnTimeout);
      warnTimeout = null;
    }
  };

  return (
    <>
      {showModal ?
        <Modal backdrop="static" show={showModal}>
          <Modal.Header>
            <Modal.Title>Session Expiring</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <p>You have been inactive for 30 minutes. Continue session?</p>
            <Counter onCountEnd={logout}/>
          </Modal.Body>
          <Modal.Footer>
            <Button
              key={0}
              variant="danger"
              onClick={logout}>
              Logout
            </Button>
            <Button
              key={1}
              variant="success"
              onClick={stayLoggedIn}>
              Back to DX
            </Button>
          </Modal.Footer>
        </Modal>
        : null}
    </>
  )
}

export default IdleTimer;
