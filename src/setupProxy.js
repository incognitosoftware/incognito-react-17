const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function(app) {
  //todo: read server host and port from environment config
  app.use(
    '/SACRestApi/api/*',
    createProxyMiddleware({
      target: 'http://10.128.21.100:2800',
      changeOrigin: true,
    })
  );
};

//target: 'http://172.20.4.33:2800',
//      target: 'http://172.20.3.21:2800',
