import React, {
  useState
} from 'react'
import {
  authenticate
} from '../shared/data/DataApi';
import PropTypes from 'prop-types';
import Container from 'react-bootstrap/Container'
import Toast from 'react-bootstrap/Toast'
import DxFormBuilder from '../shared/forms/DxFormBuilder'
import loginForm from '../json/login/login.json';

const DxLoginPage = (props) => {
  const [show, setShow] = useState(false);
  const [errorMsg, setErrorMsg] = useState();

  const fetchAuthToken = async (values, callback) => {
    try {
      const resp = await authenticate(props.headers, values);
      if (resp.error) {
        setShow(true);
        setErrorMsg(resp.data.error.description);
        return;
      }

      props.setAuthToken(resp);
    } catch (error) {
      setShow(true);
      //todo: Add a proper message
      setErrorMsg('Something went wrong. Please try again.');
    } finally {
      callback();
    }
  }

  return (
    <div className="incognito-route-container">
      <Container>
        <div className="incognito-login-app-header"/>
      </Container>
      <div className="incognito-login-page-container">
        <Container>
          <div className="incognito-login-container">
            <div className={"incognito-login-app-logo"}/>
            <div className={"incognito-login-heading"}>
              IoT Control Center
            </div>
            <DxFormBuilder {...props} token={props.token} fields={loginForm} submitFunc={fetchAuthToken}/>
          </div>
        </Container>
      </div>
      <Toast onClose={() => setShow(false)} bg="danger" show={show} delay={3000} autohide>
        <Toast.Body>{errorMsg}</Toast.Body>
      </Toast>
    </div>
  );

}

export default DxLoginPage;

DxLoginPage.propTypes = {
  setAuthToken: PropTypes.func.isRequired
}
