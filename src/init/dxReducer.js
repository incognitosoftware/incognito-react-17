const dxReducer = (state, action) => {
  if (action.type === 'login') {
    sessionStorage.setItem('token', action.payload);
    sessionStorage.removeItem('_forceLogout');

    return { ...state, token: action.payload, showNoAuthModal: false };
  }

  if (action.type === 'logout') {
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('_forceLogout');

    return { ...state, token: null, showNoAuthModal: false };
  }

  if (action.type === 'toggleNoAuthModal') {
    sessionStorage.setItem('_forceLogout', 'true');

    return { ...state, showNoAuthModal: !state.showNoAuthModal };
  }

  return state;
}

export default dxReducer;
