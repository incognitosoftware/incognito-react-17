const dxState = {
  screenWidth:       window.innerWidth,
  token:             sessionStorage.getItem('token'),
  toggleNoAuthModal: false
}

export default dxState;
