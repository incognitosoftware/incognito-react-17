const dxActions = {
  login:             { type: 'login', payload: null },
  logout:            { type: 'logout' },
  toggleNoAuthModal: { type: 'toggleNoAuthModal'}
};

export default dxActions;
