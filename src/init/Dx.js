import React, {
  useEffect,
  useReducer
} from 'react'
import {
  Switch,
  Redirect,
  withRouter
} from 'react-router-dom';
import {
  RecoilRoot
} from 'recoil'
import DxNavHeader from '../shared/nav/DxNavHeader'
import DxNavFooter from '../shared/nav/DxNavFooter'
import DxLoginPage from '../login/DxLoginPage'
import Devices from '../routes/devices/Devices'
import DeviceModels from '../routes/devicemodels/DeviceModels'
import DxHub from '../routes/hub/DxHub'
import DxPrivateRoute from '../shared/nav/DxPrivateRoute'
import IdleTimer from '../shared/IdleTimer/IdleTimer';
import dxReducer from './dxReducer'
import dxState from './dxState'
import dxActions from './dxActions'
import Accounts from "../routes/accounts/Accounts";
import Account from "../routes/accounts/Account";

const Dx = (props) => {
  const [state, dispatch] = useReducer(dxReducer, dxState);

  useEffect(() => {
    if (sessionStorage.getItem('_forceLogout')) {
      dispatch(dxActions.logout);
    }
  }, []);

  const setAuthToken = (resp) => {
    if (!resp || resp.error) {
      dispatch(dxActions.logout);
      return;
    }
    dxActions.login.payload = resp.data.authorization;
    dispatch(dxActions.login);
  };

  const forceNoAuthLogout = () => {
    dispatch(dxActions.logout);
  };

  const stayLoggedIn = () => {
    dispatch(dxActions.login);
  };

  // todo: all valid routes must be listed here as DxPrivateRoute

  return (
    <RecoilRoot>
      {sessionStorage.getItem('token') ? <IdleTimer
        {...props}
        showModal={state.showNoAuthModal}
        forceNoAuthLogout={forceNoAuthLogout}
        stayLoggedIn={stayLoggedIn}>
      </IdleTimer> : null}
      <DxNavHeader
        {...props}
        headers={props.headers}
        language={props.language}
        token={state.token}
        setAuthToken={setAuthToken}
        forceNoAuthLogout={forceNoAuthLogout}
      />
      {!state.token ? <DxLoginPage {...props} token={state.token} setAuthToken={setAuthToken}/> :
        <>
          <Switch>
            <DxPrivateRoute {...props} exact path='/dx/devices' token={state.token} component={Devices}/>
            <DxPrivateRoute {...props} exact path='/dx/accounts' token={state.token} component={Accounts}/>
            <DxPrivateRoute {...props} exact path='/dx/accounts/:id' token={state.token} component={Account}/>
            <DxPrivateRoute {...props} path='/dx/devicemodels/hub' token={state.token} component={DeviceModels}/>
            <DxPrivateRoute {...props} path='/dx/hub' token={state.token} component={DxHub}/>
            <Redirect from='/' to={'/dx/hub'}/>
          </Switch>
          <DxNavFooter {...props} />
        </>
      }
    </RecoilRoot>
  );
};

export default withRouter(Dx);
