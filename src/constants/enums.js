export const TABLE_SELECT_TYPES = Object.freeze({
  ALL: 'ALL',
  NONE: 'NONE',
  PARTIAL: 'PARTIAL'
});
