export const BASE_URL = process.env.REACT_APP_BASE_URL;
export const URL_AUTHENTICATION = "authentication";
export const URL_DEVICES = "devices";
export const URL_ACCOUNTS = "accounts";
export const URL_ACCOUNT = "accounts/identifier";
